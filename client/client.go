package client

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/hashicorp/consul/connect"
)

type Client struct {
	http     *http.Client
	baseAddr string
}

func NewClient(url string) *Client {
	client := &http.Client{
		Transport: http.DefaultTransport,
		Timeout:   30 * time.Second,
	}

	return NewHTTPClient(client, url)
}

func NewHTTPClient(client *http.Client, url string) *Client {
	return &Client{
		http:     client,
		baseAddr: strings.TrimRight(url, "/"),
	}
}

func NewConsulClient(service *connect.Service, url string) *Client {
	return NewHTTPClient(service.HTTPClient(), url)
}

// XID returns a new unique XID identified.
func (c Client) XID() (string, error) {
	url := fmt.Sprintf("%s/xid", c.baseAddr)

	resp, err := c.http.Get(url)
	if err != nil {
		return "", fmt.Errorf("could not get: %w", err)
	}
	defer func() {
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected status %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	return string(body), err
}
