package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/hashicorp/consul/api"
	"github.com/hashicorp/consul/connect"
	"github.com/namsral/flag"
	"github.com/rs/xid"
)

var (
	httpBind      = flag.String("http-bind", "127.0.0.1:8080", "http binding address")
	consulConnect = flag.Bool("consul-connect", false, "enable consul connect native mode")
)

var ids = map[string]http.HandlerFunc{
	"/xid": xidHandler,
}

func main() {
	flag.Parse()

	service, err := setupConsul()
	if err != nil {
		log.Fatalf("could not setup consul: %v", err)
	}
	defer service.Close()

	server := &http.Server{
		Addr:              *httpBind,
		ReadHeaderTimeout: 10 * time.Second,
		ReadTimeout:       30 * time.Second,
		WriteTimeout:      1 * time.Minute,
		Handler:           serverMux(),
	}

	if *consulConnect {
		log.Print("running in consul connect mode")
		server.TLSConfig = service.ServerTLSConfig()
		log.Fatal(server.ListenAndServeTLS("", ""))
	} else {
		log.Print("running in legacy mode")
		log.Fatal(server.ListenAndServe())
	}
}

func serverMux() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		handler, ok := ids[r.URL.Path]
		if !ok {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		handler.ServeHTTP(w, r)
	})
	return mux
}

func xidHandler(w http.ResponseWriter, r *http.Request) {
	id := xid.New().String()
	w.Write([]byte(id))
}

func setupConsul() (*connect.Service, error) {
	client, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		return nil, fmt.Errorf("could not create client: %w", err)
	}

	service, err := connect.NewService("uid-api", client)
	if err != nil {
		return nil, fmt.Errorf("could not create service: %w", err)
	}

	return service, nil
}
