module gitlab.com/trilliot/uid-api

go 1.15

require (
	github.com/hashicorp/consul v1.9.4
	github.com/hashicorp/consul/api v1.8.1
	github.com/namsral/flag v1.7.4-pre
	github.com/rs/xid v1.3.0
)
